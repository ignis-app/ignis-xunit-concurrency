﻿using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Ignis.Xunit.Currency.Tests.Fixture
{
    public sealed class シンプルなテスト
    {
        private readonly ITestOutputHelper _output;

        public シンプルなテスト(ITestOutputHelper output)
        {
            _output = output;
        }

        public static Reporter Reporter { get; private set; } = new();

#pragma warning disable xUnit1013
        public static void Initialize()
#pragma warning restore xUnit1013
        {
            Reporter = new Reporter();
        }

        [Fact]
        public async Task Test1()
        {
            _output.WriteLine(nameof(Test1));

            Reporter.Log($"Start {nameof(Test1)}");

            await Task.Delay(1000);

            Reporter.Log($"End {nameof(Test1)}");
        }

        [Fact]
        public async Task Test2()
        {
            _output.WriteLine(nameof(Test2));

            await Task.Delay(100);

            Reporter.Log($"Start {nameof(Test2)}");

            await Task.Delay(1000);

            Reporter.Log($"End {nameof(Test2)}");
        }
    }
}
