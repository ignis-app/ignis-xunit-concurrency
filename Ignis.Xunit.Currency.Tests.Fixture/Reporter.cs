﻿using System.Text;

namespace Ignis.Xunit.Currency.Tests.Fixture
{
    public sealed class Reporter
    {
        private readonly StringBuilder _builder = new();

        // ReSharper disable once UnusedMethodReturnValue.Global
        public Reporter Log(string line)
        {
            _builder.AppendLine(line);
            return this;
        }

        public override string ToString()
        {
            return _builder.ToString().TrimEnd();
        }
    }
}
