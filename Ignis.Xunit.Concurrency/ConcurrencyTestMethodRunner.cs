﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Ignis.Xunit.Concurrency
{
    public sealed class ConcurrencyTestMethodRunner : XunitTestMethodRunner
    {
        public ConcurrencyTestMethodRunner(ITestMethod testMethod,
            IReflectionTypeInfo @class,
            IReflectionMethodInfo method,
            IEnumerable<IXunitTestCase> testCases,
            IMessageSink diagnosticMessageSink,
            IMessageBus messageBus,
            ExceptionAggregator aggregator,
            CancellationTokenSource cancellationTokenSource,
            object[] constructorArguments) :
            base(testMethod,
                @class,
                method,
                testCases,
                diagnosticMessageSink,
                messageBus,
                aggregator,
                cancellationTokenSource,
                constructorArguments)
        {
        }

        protected override async Task<RunSummary> RunTestCasesAsync()
        {
            return await Concurrency.Create(TestCases)
                .RunAsync(RunTestCaseAsync);
        }
    }
}
