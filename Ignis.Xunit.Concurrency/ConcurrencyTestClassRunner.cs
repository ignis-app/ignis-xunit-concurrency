﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Ignis.Xunit.Concurrency
{
    public sealed class ConcurrencyTestClassRunner : XunitTestClassRunner
    {
        public ConcurrencyTestClassRunner(ITestClass testClass,
            IReflectionTypeInfo @class,
            IEnumerable<IXunitTestCase> testCases,
            IMessageSink diagnosticMessageSink,
            IMessageBus messageBus,
            ITestCaseOrderer testCaseOrderer,
            ExceptionAggregator aggregator,
            CancellationTokenSource cancellationTokenSource,
            IDictionary<Type, object> collectionFixtureMappings) :
            base(testClass,
                @class,
                testCases,
                diagnosticMessageSink,
                messageBus,
                testCaseOrderer,
                aggregator,
                cancellationTokenSource,
                collectionFixtureMappings)
        {
        }

        protected override async Task<RunSummary> RunTestMethodsAsync()
        {
            // TODO 例外をログに記録する必要あり!
            // TODO Cancel できるように?

            var source = TestCaseOrderer.OrderTestCases<IXunitTestCase>(TestCases);

            var constructorArguments = CreateTestClassConstructorArguments();
            var groups = source.GroupBy((Func<IXunitTestCase, ITestMethod>) (tc => tc.TestMethod),
                TestMethodComparer.Instance);

            return await Concurrency.Create(groups)
                .RunAsync(grouping => RunTestMethodAsync(grouping.Key,
                    (IReflectionMethodInfo) grouping.Key.Method,
                    grouping,
                    constructorArguments));
        }

        protected override async Task<RunSummary> RunTestMethodAsync(ITestMethod testMethod,
            IReflectionMethodInfo method,
            IEnumerable<IXunitTestCase> testCases,
            object[] constructorArguments)
        {
            var runner = new ConcurrencyTestMethodRunner(testMethod,
                Class,
                method,
                testCases,
                DiagnosticMessageSink,
                MessageBus,
                new ExceptionAggregator(Aggregator),
                CancellationTokenSource,
                constructorArguments);

            return await runner.RunAsync();
        }
    }
}
