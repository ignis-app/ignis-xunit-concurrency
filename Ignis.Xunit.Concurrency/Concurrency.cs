﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Ignis.Xunit.Concurrency
{
    internal static class Concurrency
    {
        public static Concurrency<T> Create<T>(IEnumerable<T> items)
        {
            return new(items);
        }
    }

    internal sealed class Concurrency<T>
    {
        private readonly IEnumerable<T> _items;

        public Concurrency(IEnumerable<T> items)
        {
            _items = items.ToImmutableArray();
        }

        public async Task<RunSummary> RunAsync(Func<T, Task<RunSummary>> function)
        {
            var tasks = _items.Select(function).ToImmutableArray();

            var summary = new RunSummary();
            foreach (var task in tasks) summary.Aggregate(await task);

            return summary;
        }
    }
}
