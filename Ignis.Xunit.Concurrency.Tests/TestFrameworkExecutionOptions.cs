﻿using System.Collections.Generic;
using Xunit.Abstractions;

namespace Ignis.Xunit.Concurrency.Tests
{
    internal sealed class TestFrameworkExecutionOptions : ITestFrameworkExecutionOptions
    {
        private readonly IDictionary<string, object?> _values = new Dictionary<string, object?>();

        public TValue? GetValue<TValue>(string name)
        {
            if (_values.TryGetValue(name, out var value)) return (TValue?) value;

            return default;
        }

        public void SetValue<TValue>(string name, TValue? value)
        {
            _values[name] = value;
        }
    }
}
