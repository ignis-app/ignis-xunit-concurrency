﻿using System;
using System.Linq.Expressions;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Ignis.Xunit.Concurrency.Tests.Wrapper
{
    internal sealed class TestClassWrapper<TClass> where TClass : class
    {
        private readonly IMessageSink _messageSink;
        private readonly ReflectionTypeInfo _type;

        public TestClassWrapper() : this(new NullMessageSink())
        {
        }

        private TestClassWrapper(IMessageSink messageSink)
        {
            _messageSink = messageSink;
            _type = new ReflectionTypeInfo(typeof(TClass));

            TestAssembly = new TestAssembly(new ReflectionAssemblyInfo(typeof(TClass).Assembly));
            var collections = new CollectionPerAssemblyTestCollectionFactory(TestAssembly, messageSink);
            var collection = collections.Get(_type);
            TestClass = new TestClass(collection, _type);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public TestClass TestClass { get; }

        public ITestAssembly TestAssembly { get; }

        public TestCaseWrapper GetTestCase(Expression<Action<TClass>> testMethod)
        {
            var method = (MethodCallExpression) testMethod.Body;
            return GetTestCase(method.Method.Name);
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public TestCaseWrapper GetTestCase(string testMethodName)
        {
            var testMethod = new TestMethod(TestClass, _type.GetMethod(testMethodName, false));
            return new TestCaseWrapper(_messageSink, testMethod);
        }
    }
}
