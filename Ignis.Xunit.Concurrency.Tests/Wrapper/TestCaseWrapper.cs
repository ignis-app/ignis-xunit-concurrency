﻿using System;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Ignis.Xunit.Concurrency.Tests.Wrapper
{
    internal sealed class TestCaseWrapper
    {
        public TestCaseWrapper(IMessageSink messageSink, TestMethod testMethod)
        {
            TestCase = new XunitTestCase(messageSink,
                TestMethodDisplay.Method,
                TestMethodDisplayOptions.All, testMethod,
                Array.Empty<object>());
        }

        public XunitTestCase TestCase { get; }
    }
}
