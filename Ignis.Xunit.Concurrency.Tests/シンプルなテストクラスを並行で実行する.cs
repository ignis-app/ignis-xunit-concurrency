﻿using System.Threading.Tasks;
using Ignis.Xunit.Concurrency.Tests.Wrapper;
using Ignis.Xunit.Currency.Tests.Fixture;
using PowerAssert;
using Xunit;
using Xunit.Sdk;

namespace Ignis.Xunit.Concurrency.Tests
{
    public sealed class シンプルなテストクラスを並行で実行する
    {
        private readonly NullMessageSink _diagnosticMessageSink;
        private readonly TestClassWrapper<シンプルなテスト> _testClass;

        public シンプルなテストクラスを並行で実行する()
        {
            _diagnosticMessageSink = new NullMessageSink();

            _testClass = new TestClassWrapper<シンプルなテスト>();
        }

        [Fact]
        public async Task Test()
        {
            シンプルなテスト.Initialize();

            var testCases = new[]
            {
                _testClass.GetTestCase(p => p.Test1()).TestCase,
                _testClass.GetTestCase(p => p.Test2()).TestCase
            };

            var target = new ConcurrencyTestAssemblyRunner(_testClass.TestAssembly,
                testCases,
                _diagnosticMessageSink,
                new NullMessageSink(),
                new TestFrameworkExecutionOptions());

            await target.RunAsync();

            var expected = @"
Start Test1
Start Test2
End Test1
End Test2
".Trim();

            PAssert.IsTrue(() => シンプルなテスト.Reporter.ToString() == expected);
        }
    }
}
