using System.IO;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.Execution;
using Nuke.Common.Git;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.ReSharper;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

[CheckBuildProjectConfigurations]
[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild
{
    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    readonly Configuration Configuration = IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [GitRepository] readonly GitRepository GitRepository;
    [GitVersion(Framework = "net5.0")] readonly GitVersion GitVersion;

    [PackageExecutable("resharper-to-codeclimate", "resharper-to-codeclimate.dll", Framework = "net5.0")]
    readonly Tool ReSharperToCodeClimate;

    [Parameter("Skip compile - Default is false.")] readonly bool SkipCompile;

    [Solution] readonly Solution Solution;

    AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

    Target Clean => _ => _
        .Before(Restore)
        .Executes(() =>
        {
            EnsureCleanDirectory(ArtifactsDirectory);
        });

    Target Restore => _ => _
        .OnlyWhenDynamic(() => !SkipCompile)
        .Executes(() =>
        {
            DotNetRestore(s => s
                .SetProjectFile(Solution));
        });

    Target Compile => _ => _
        .OnlyWhenDynamic(() => !SkipCompile)
        .DependsOn(Restore)
        .Executes(() =>
        {
            DotNetBuild(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .SetProperty("TreatWarningsAsErrors", true)
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetFileVersion(GitVersion.AssemblySemFileVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .EnableNoRestore());
        });

    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var testProjects = Solution.GetProjects("*.Tests");

            DotNetTest(s => s
                .SetConfiguration(Configuration)
                .SetTestAdapterPath(".")
                .SetLogger(
                    @$"junit;LogFilePath={ArtifactsDirectory}/{{assembly}}-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose")
                .EnableNoBuild()
                .CombineWith(testProjects, (p, project) => p.SetProjectFile(project)));
        });

    Target Lint => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var xml = ArtifactsDirectory / "inspection-report.xml";
            var json = ArtifactsDirectory / "inspection-report.json";

            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .SetProperty("Configuration", Configuration)
                .SetOutput(xml));

            ReSharperToCodeClimate($"{xml} {json}");

            if (File.ReadAllText(xml).Contains("Severity")) ControlFlow.Fail("Found code problems.");
        });

    /// Support plugins are available for:
    /// - JetBrains ReSharper        https://nuke.build/resharper
    /// - JetBrains Rider            https://nuke.build/rider
    /// - Microsoft VisualStudio     https://nuke.build/visualstudio
    /// - Microsoft VSCode           https://nuke.build/vscode
    public static int Main() => Execute<Build>(x => x.Compile);
}
